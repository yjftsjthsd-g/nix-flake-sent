# nix-flake-sent

A nix flake for suckless sent.
(Because I needed a trivial X11 program to practice packaging.)


## Use

On a flake-enabled nix system (see https://nixos.wiki/wiki/Flakes), either:

Clone this repo and
```
nix build
./result/bin/sent ./result/share/sent/example
```
(Note that this is an example from the upstream repo but with all images
stripped out, which makes some slides nonsensical but was good enough for me to
verify that the program actually worked.)

Or just run directly with
```
nix run gitlab:yjftsjthsd-g/nix-flake-sent -- presentation-file
```

You can also run a specific version by its git tag:
```
nix run gitlab:yjftsjthsd-g/nix-flake-sent/v1 -- presentation-file
```
Although of course this only works for versions that I've actually tagged
(https://gitlab.com/yjftsjthsd-g/nix-flake-sent/-/tags)


## Features

Note that I have made no effort to support farbfeld images, so I don't know what
the state of picture support is in this build.


## License

The contents of this repo is under the ISC license (see LICENSE file), and the
sent progam is also under the ISC license.

