{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation rec {
    name = "sent";
    version = "1";
    src = fetchurl {
        url = "https://dl.suckless.org/tools/sent-${version}.tar.gz";
        hash = {
            "1" = "sha256-e/PekxHOKR/zat9TFbePpyNz46uAynE4f7WIS8vXvjM=";
        }.${version} or "sha256-0000000000000000000000000000000000000000000=";
    };
    buildInputs = [
        pkgs.xlibs.libX11
            pkgs.xlibs.libXft
    ];
    unpackPhase =
        ''
        mkdir sent
        cd sent
        tar xzf $src
        '';
    buildPhase = "make PREFIX=$out";
    installPhase =
        ''
        make install PREFIX=$out
        mkdir $out/share/sent
#cp nyan.png transparent_test.ff example $out/share/sent/
        grep -v ^@ example > $out/share/sent/example
        '';

    meta = {
        description = "Simple plaintext presentation tool.";
        homepage = "https://tools.suckless.org/sent/";
        license = "ISC";
    };
}
